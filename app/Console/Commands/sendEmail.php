<?php

namespace App\Console\Commands;

use App\user;
use Illuminate\Console\Command;

class sendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:name';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send email to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = user::all();
        $users->map(function($user){
            $messages = GetThreadMessages::new($user);
            $user->notify (new UnreadMessagesInThread($messages, $user));
        });
    }
}

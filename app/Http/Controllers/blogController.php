<?php

namespace App\Http\Controllers;

use App\comments;
use Illuminate\Http\Request;
use App\blog;

class blogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['index','show']]);
    }

    public function index()
    {
        $data = blog::orderBy('created_at','desc')->get();
        return view('blog.index')->with('data',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this -> validate($request,[
            'title' => 'required',
            'body' => 'required',
        ]);

        $blog = new blog;
        $blog -> title = $request -> input('title');
        $blog -> body = $request -> input('body');
        $blog -> user_id = auth()->user()->id;
        $blog -> save();

        //notification
        $notification = array(
          'message' => 'blog Created!',
          'alert-type' => 'success',
        );

        return redirect(route('blog.index')) ->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = blog::findOrFail($id);
        return view('blog.show', compact('data'));
//        return view('blog.show')->with('data',$data)->with('comments',$comments);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = blog::findOrFail($id);
        return view('blog.edit') -> with('data',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'title' => 'required',
            'body' => 'required',
        ]);

        $blog = blog::findOrFail($id);
        if(auth()->user()->id==$blog->user_id) {
            $blog->title = $request->input('title');
            $blog->body = $request->input('body');
            $blog->save();

            //notification
            $notification = array(
                'message' => 'Update Successful!',
                'alert-type' => 'success',
            );

            return redirect(route('blog.index'))->with($notification);
        }
        else{

            //notification
            $notification = array(
                'message' => 'Update Unsuccessful!',
                'alert-type' => 'error',
            );
            return redirect(route('blog.index'))->with($notification);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $blog = blog::findOrFail($id);
        if($blog->user_id==auth()->user()->id){
            $blog -> delete();

            //notification
            $notification = array(
                'message' => 'Delete Successful!',
                'alert-type' => 'success',
            );

            return redirect(route('blog.index')) -> with($notification);
        }
        else{
            //notification
            $notification = array(
                'message' => 'Delete Unsuccessful!',
                'alert-type' => 'error',
            );

            return redirect(route('blog.index'))->with($notification);
}
    }
}

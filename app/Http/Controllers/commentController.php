<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\comments;
use function Sodium\compare;

class commentController extends Controller
{
    //constructor
    public function __construct()
    {
        $this->middleware('auth',['except'=>['index']]);
    }


    //store
    public function store(Request $request){
        //validate
        $this->validate($request,[
            'body' => 'required'
        ]);

        //create new record
        $comment = new comments();
        $comment->blog_id = $request->input('id');
        $comment->user_id = auth()->user()->id;
        $comment->body = $request->input('body');
        $comment->save();

        //notification
        $notification = array(
            'message' => 'Comment added!',
            'alert-type' => 'success',
        );

        //redirect
        return redirect(route('blog.show',['id'=>$comment->blog_id]))->with($notification);
    }

    //edit
    public function edit($id){
        //get data
        $comment = comments::find($id);
        //return view
        return view('comment.edit')->with('comment',$comment);
    }


    //update
    public function update(Request $request,$id){
        //validate
        $this->validate($request,[
            'body' => 'required'
        ]);

        //find record
        $comment = comments::findOrFail($id);
        if($comment->user_id == auth()->user()->id){
            //update
            $comment->body = $request->input('body');
            $comment->save();

            //notification
            $notification = array(
                'message' => 'Comment Updated!',
                'alert-type' => 'success',
            );

            return redirect(route('blog.show',['id'=>$comment->blog_id]))->with($notification);
        }
        else {
            //redirect, unsuccessful
            $notification = array(
                'message' => 'Unauthorize Access!',
                'alert-type' => 'error',
            );

            return redirect(route('blog.show',['id'=>$comment->blog_id]))->with($notification);
        }
    }

    //destroy
    public function destroy($id){
        //find record
        $comment = comments::findOrFail($id);
        if($comment->user_id == auth()->user()->id ){
            //delete
            $comment->delete();

            //notification
            $notification = array(
                'message' => 'Comment Deleted Successfully!',
                'alert-type' => 'success',
            );

            return redirect(route('blog.show',['id'=>$comment->blog_id]))->with($notification);
        }
        else {
            //redirect, unsuccessful
            $notification = array(
                'message' => 'Unauthorize Access!',
                'alert-type' => 'error',
            );
            return redirect(route('blog.show',['id'=>$comment->blog_id]))->with($notification);
        }
    }

}

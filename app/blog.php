<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blog extends Model
{
    protected $table = 'blogs';
    protected $primaryKey ='id';
    public $timestamps = 'true';

    public function user(){
        return $this->belongsTo(user::class);
    }

//    public function comments(){
//        return $this->hasMany('App\comments');
//    }
    public function comments(){
        return $this->hasMany(comments::class);
    }

}

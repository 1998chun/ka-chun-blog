<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comments extends Model
{
    protected $table = 'comments';
    protected $primaryKey ='id';
    public $timestamps = 'true';

    public function user(){
        return $this->belongsTo(user::class);
    }

    public function blog(){
        return $this->belongsTo(blog::class);
    }
}

@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="page-header">Create</h1>
        <form method="POST" action="{{ route('blog.store') }}">
            @csrf
            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" placeholder="Title" id="title" name="title" type="text" >
            </div>
            <div class="form-group">
                <label for="body">Body</label>
                <textarea class="form-control" placeholder="body" id="body" name="body" ></textarea>
            </div>
            <a href="{{route('blog.index')}}" class="btn btn-secondary float-left">Go Back</a>
            <button class="btn btn-success float-right" type="submit">Save</button>
        </form>
    </div>

{{--    {{Form::textarea('body', '',['id' => 'article-ckeditor'])}}--}}

@endsection

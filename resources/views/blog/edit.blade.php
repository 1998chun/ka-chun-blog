@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="page-header">Edit</h1>
        <form action="{{route('blog.update',['id'=>$data->id])}}" method="POST">
            @csrf
            <input type="hidden" name="_method" value="PUT">
            <div class="form-group">
                <label for="title">Title</label>
                <input class="form-control" placeholder="title" type="text" id="title" name="title" value="{{$data->title}}">
            </div>
            <div class="form-group">
                <label for="body">Body</label>
                <textarea class="form-control" placeholder="body" id="body" name="body">{{$data->body}}</textarea>
            </div>
            <input type="hidden" name="_method" value="PUT">
            <button type="submit" class="btn btn-success">Update</button>
        </form>
        <a class="btn btn-secondary" href="{{route('blog.show',['id'=>$data->id])}}">Go Back</a>
    </div>


{{--    {{Form::label('body', $data->body, ['id'=>'article-ckeditor'])}}--}}


@endsection
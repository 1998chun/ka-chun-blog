@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="page-header float-left">Blog Feed</h1>
        <a class="btn btn-success float-right" href="{{route('blog.create')}}">Create Blog</a>
        @if(count($data)>0)
            <table class="table table-striped">
                <tr>
                    <th>Title</th>
                    <th>Created by</th>
                    <th>Written on</th>
                    <th></th>
                    <th></th>
                </tr>
            @foreach($data as $blog)
                <tr>
                    <td><a href="{{route('blog.show',['id' => $blog->id])}}">
                            {{$blog->title}}</a></td>
                    <td>{{$blog->user->name}}</td>
                    <td>{{$blog->created_at}}</td>
                    <td><a href="{{route('blog.edit',['id' => $blog->id])}}"></a></td>
                    <td><a href="{{route('blog.destroy',['id' => $blog->id])}}"></a></td>
                </tr>
            @endforeach
            </table>
        @else
            <h1 class="page-header">Oopss.. There's no blog post yet!</h1>
        @endif
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="container">

        <section>
            <div class="clearfix">
                <div class="float-left">
                    <h1 class="page-header">{{ $data->title }}</h1>
                    <small>Written on {{$data->created_at}} Created by <b>{{$data->user->name}}</b></small>
                </div>
                @if($data->user_id==auth()->user()->id)
                    <div class="float-right">
                        <div class="inline">
                            <a href="{{route('blog.edit',['id'=>$data->id])}}" class="btn btn-success">Edit</a>
                        </div>
                        <div class="inline">
                            <form action="{{route('blog.destroy',['id'=>$data->id])}}" method="POST">
                                @csrf
                                <input type="hidden" name="_method" value="DELETE">
                                <button type="submit" class="btn btn-danger inline">Delete</button>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
            <article>
                <p>{{$data->body}}</p>
            </article>
        </section>
        <section id="comment">
            <form action="{{route('comment.store',['id'=>$data->id])}}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{$data->id}}">
                <div class="form-group">
                    <label for="body">Say something here..</label>
                    <textarea id="body" class="form-control" name="body" required></textarea>
                </div>
                <button class="btn btn-default float-right" style="margin-bottom: 10px" type="submit">Comment</button>
            </form>
            <table class="table table-striped">
                <tr>
                    <th>Comments</th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            @foreach($data->comments as $comment)
                <tr>
                    <td>{{$comment->user->name}} says</td>
                    <td>{{$comment->body}}</td>
                    @if($comment->user_id == auth()->user()->id)
                        <td><a href="{{route('comment.edit',['comment_id'=>$comment->id])}}" class="btn btn-success">Edit</a></td>
                        <td>
                            <form action="{{route('comment.destroy',['id'=>$comment->id])}}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    @else
                        <td></td>
                        <td></td>
                    @endif
                </tr>

            @endforeach
            </table>
        </section>
        <br>
        <!--Go back function -->
        <a href="{{route('blog.index')}}" class="btn btn-secondary">Go Back</a>
    </div>

@endsection

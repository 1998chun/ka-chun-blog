@include('layouts.app')

<form action="{{route('comment.update',['comment_id'=>$comment->id])}}" method="POST">
    @csrf
    @method('PUT')
    <label for="body"><h1>Edit Your Comment</h1></label>
    <textarea id="body" name="body" required>{{$comment->body}}</textarea>
    <a href="{{route('blog.show',['id'=>$comment->blog_id])}}" class="btn btn-danger">Cancel</a>
    <button class="btn btn-success" type="submit">Submit</button>
</form>
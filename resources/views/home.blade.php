@extends('layouts.app')

@section('content')
<div class="container">
    <a href="{{route('blog.create')}}" class="btn btn-success">Create</a>
    <a href="{{route('blog.index')}}" class="btn btn-dark">View All Post</a>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    You are logged in! See own post here
                </div>
            </div>
            <br><br>
            <div class="card">
                @if(count($data)>0)
                    <table class="table table-striped">
                        <tr>
                            <th>Title</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($data as $blog)
                            <tr>
                                <td><a href="{{route('blog.show',['id'=>$blog->id])}}">{{$blog->title}}</a></td>
                                <td><a href="{{route('blog.edit',['id'=>$blog->id])}}" class="btn btn-success">Edit</a></td>
                                <td>
                                    <form action="{{route('blog.destroy',['id'=>$blog->id])}}" method="POST">
                                        @method('DELETE')
                                        @csrf
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection

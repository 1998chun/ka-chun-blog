    @if(count($errors)>0)
        @foreach($errors as $error)
            <div class="alert alert-danger">
                {{$error}}
            </div>
        @endforeach
    @endif

{{--    @if(session('success'))--}}
{{--        <div class="alert alert-success">--}}
{{--            {{session('success')}}--}}
{{--        </div>--}}
{{--    @endif--}}

{{--    @if(session('error'))--}}
{{--        <div class="alert alert-danger">--}}
{{--            {{session('error')}}--}}
{{--        </div>--}}
{{--    @endif--}}

    <script>
        @if(Session::has('message'))
            var type = "{{Session::get('alert-type','info')}}";
            switch (type) {
                case 'info':
                    toastr.info("{{Session::get('message')}}");
                    break;
                case 'warning':
                    toastr.warning("{{Session::get('message')}}");
                    break;
                case 'success':
                    toastr.success("{{Session::get('message')}}");
                    break;
                case 'error':
                    toastr.error("{{Session::get('message')}}");
                    break;
            }
        @endif
    </script>

    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

@component('mail::message')
#Hello {{$user->name}}

<p>you have new reply(s) in your thread(s)</p>

@foreach($messages as $message)
    <p><strong>{{ $message->thread }}</strong></p>
    <p>{{ $message->body }}</p>
    <small>Replied <strong>{{ \Carbon\Carbon::parse($message->created_at)->diffForHumans() }}</strong></small>
@endforeach

@component('mail::button', ['url' => ''])
Button Text
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('blog', 'blogController');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

//comment controller
Route::GET('/blog/comment/{comment_id}','commentController@edit')->name('comment.edit');
Route::POST('/blog/{id}/comment','commentController@store')->name('comment.store');
Route::PUT('/blog/comment/{comment_id}','commentController@update')->name('comment.update');
Route::DELETE('/blog/comment/{comment_id}','commentController@destroy')->name('comment.destroy');

//blog controller
Route::get('/blog', 'blogController@index')->name('blog.index');
Route::get('/blog/create', 'blogController@create')->name('blog.create');
Route::POST('/blog', 'blogController@store')->name('blog.store');
Route::get('/blog/{id}', 'blogController@show')->name('blog.show');
Route::get('/blog/{id}/edit', 'blogController@edit')->name('blog.edit');
Route::PUT('/blog/{id}', 'blogController@update')->name('blog.update');
Route::DELETE('/blog/{id}', 'blogController@destroy')->name('blog.destroy');
